package cat.itb.m3;

import java.util.Scanner;

public class LlegirSencersAmbNextInt {
    public static void main(String[] args) {
        //Declarem l'objecte entrada
        Scanner entrada = new Scanner(System.in);
        int dia, mes, any; //creem les variables que utilitzarem

        System.out.println("Introdueiex separt per espais, dia, mes i l'any actual' (p.ex: 27 10 2020)");
        dia = entrada.nextInt();
        mes = entrada.nextInt();
        any = entrada.nextInt();

        System.out.println("La data introduida és: "+dia+"/"+mes+"/"+any);

    }
}
