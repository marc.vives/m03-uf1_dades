package cat.itb.m3;

import java.util.Scanner;

public class LlegirTextIsensers {

    public static void main(String[] args) {
        //Declarem l'objecte entrada
        Scanner entrada = new Scanner(System.in);
        String nom;
        int edat;

        //Demanem a l'usuari que introdueixi el seu nom.
        System.out.println("Hola, com et dius? (acaba amb un intro)");
        //amb entrada.nextLine(), es llegeix la linea de text de l'usuari.
        //Podem guardar-la en un String
        nom = entrada.nextLine();

        //Mostrem un text utilitzan l'String que hem obtingut
        //per concatenar text utilitzem el signe +.
        System.out.println("Encantat de coneixe't "+nom);


        //Demanem a l'usuari que introdueixi la seva edat.
        System.out.println("Quina edat tens? (introdueix un enter i toca la tecla intro)");
        //amb entrada.nextInt(), llegim l'enter que ha introduit l'usuari.
        edat = entrada.nextInt();

        //Mostrem un text utilitzan les dades obtingudes
        System.out.println("Encantat de coneixe't "+nom+" (" +edat+ "anys)" );

    }
}
