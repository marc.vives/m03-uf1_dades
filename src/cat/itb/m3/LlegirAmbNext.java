package cat.itb.m3;

import java.util.Scanner;

public class LlegirAmbNext {
    public static void main(String[] args) {
        //Declarem l'objecte entrada
        Scanner entrada = new Scanner(System.in);
        //creem les variables que utilitzarem
        String paraula1, paraula2, paraula3, linea;
        System.out.println("Introdueix el text: Practia amb Scanner");
        paraula1 = entrada.next(); //el cursor es situa Practia |amb Scanner
        paraula2 = entrada.next(); //el cursor es situa Practia amb |Scanner
        paraula3 = entrada.next(); //el cursor es situa Practia amb Scanner|
        System.out.println(paraula1 + " " + paraula2 + " " + paraula3);
        // Si utilitzem un entrada.nextLine(), llegirem únicament un salt de linea (un intro)
        linea = entrada.nextLine();
        //sol llegeix un salt de linea el "\n", cursor a l'inici de la seguent linea d'entrada
        System.out.println("en aquesta linea sol ens queda un salt de linea= " + linea);
        // Així doncs, necessitarem un altres nextLine() per posar el cursor a la nova linea

        System.out.println("Introdueix el text: Aquesta serà una linea");
        linea = entrada.nextLine();
        System.out.println("linea = " + linea);
    }
}